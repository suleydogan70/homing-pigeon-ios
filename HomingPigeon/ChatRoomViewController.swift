//
//  ViewController.swift
//  HomingPigeon
//
//  Created by Sema Dogan on 09/07/2020.
//  Copyright © 2020 Suleyman DOGAN. All rights reserved.
//

import UIKit

class ChatRoomViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var messages: [MessageModel] = []
    
    override func viewDidLoad() {
        
        tableView.delegate = self
        tableView.dataSource = self
        // Delegate
        
        for i in 1...100 {
            messages.append(MessageModel(sender: "Macron", message: "Parce que c'est notre projet n°\(i)", hour: "22:33"))
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        
        let message = messages[indexPath.row]
        
        let senderLabel = cell.viewWithTag(1) as! UILabel
        let contentLabel = cell.viewWithTag(2) as! UILabel
        let hourLabel = cell.viewWithTag(3) as! UILabel
        
        senderLabel.text = message.senderName
        contentLabel.text = message.content
        hourLabel.text = message.hour
        
        return cell
    }
}

