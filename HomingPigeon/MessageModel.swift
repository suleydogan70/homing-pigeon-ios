//
//  MessageModel.swift
//  HomingPigeon
//
//  Created by Sema Dogan on 09/07/2020.
//  Copyright © 2020 Suleyman DOGAN. All rights reserved.
//

import Foundation

class MessageModel {
    var senderName: String
    var content: String
    var hour: String
    
    init(sender senderName: String, message content: String, hour hour: String) {
        self.senderName = senderName
        self.content = content
        self.hour = hour
    }
}
