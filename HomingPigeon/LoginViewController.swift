//
//  LoginViewController.swift
//  HomingPigeon
//
//  Created by Sema Dogan on 09/07/2020.
//  Copyright © 2020 Suleyman DOGAN. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginActivityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        print("Login View Did Load")
        
        userNameTextField.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("Login View Did Appear")
        userNameTextField.becomeFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userNameTextField.isEnabled = true
        loginButton.isHidden = false
        loginActivityIndicator.isHidden = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        performLogin()
        return true
    }
    
    @IBAction func onLoginPressed(_ sender: Any) {
        print("Connexion pressed")
        if userNameTextField.text != "" {
            performLogin()
        }
    }
    
    func performLogin () {
        userNameTextField.isEnabled = false
        loginButton.isHidden = true
        loginActivityIndicator.isHidden = false
        let seconds = 1.0

        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {

            self.performSegue(withIdentifier: "chatRoom", sender: nil)

        }
    }
}
